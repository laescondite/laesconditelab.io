---
title: "Quienes somos"
# meta description
description: "This is meta description"
# save as draft
draft: false
---

Somos un grupo de personas que trabajan por mejorar la sociedad desde el punto de vista de la soberanía digital, el respeto a los derechos humanos y la privacidad en el campo de la tecnología.


### ¿Cómo nace LaEscondite?

LaEscondite es un proyecto que nace de un pequeño grupo de personas preocupadas por estas cuestiones. Este grupo se establece por primera vez en MediaLab Prado para desarrollar **[Trackula](https://trackula.org/es/page/landing-plugin/)**. Una vez establecido, hemos continuado también en la divulgación en otros espacios. Podeis conocer más sobre el grupo [en nuestra web][trackulaweb].

[trackulaweb]: https://trackula.org/es/

El momento en el que surgió la idea de LaEscondite vino durante el congreso [esLibre 2019](https://eslib.re/2019/programa/) en donde Trackula organizó la devroom para debatir sobre *Privacidad, descentralización y soberanía digital*.la

Allí conocimos a personas de múltiples colectivos que, como nosotros, trabajan por la privacidad, la ética en tecnología y la libertad en internet y, en ese momento fue claro que faltaba un espacio en común.

Inicialmente se propuso organizar un evento pesencial, cuando no teníamos presente que una pandemia mundial nos iba requerir distanciarnos físicamente. Como una de las razones del evento es que nos conozcamos y sepamos quiénes somos, para entender cómo podríamos colaborar, creíamos que vernos en persona era lo ideal.

Sin embargo, dada la situación actual, se ha complicado un poco el plan inicial, así que decidimos empezar por una pequeña versión online para esta primera toma de contacto, tratando de mantener el espíritu de tener espacios cercanos, reducidos y seguros en donde poder debatir.

Ahora el siguiente paso es unirnos en comunidad.

#### ¿Quienes estamos detrás?


<div class="organization-flex">
  <div>
    <img src="images/h_emma.png" />
    <h5>Emma López</h5>
    <p>&nbsp;</p>
  </div>
  <div>
    <img src="images/h_pablo.png" />
    <h5>Pablo Castro</h5>

[Trackula][trweb]
  </div>
  <div>
    <img src="images/h_sofia.png" />
    <h5>Sofía Prósper</h5>

[Trackula][trweb]
  </div>
  <div>
    <img src="images/h_santiago.png" />
    <h5>Santiago Saavedra</h5>

[Trackula][trweb]
  </div>
  <div>
    <img src="images/h_virginia.png" />
    <h5>Virginia Díez</h5>

[Wikimedia España][wmes]
  </div>
</div>


[trweb]: https://trackula.org/es/page/landing-plugin/
[wmes]: https://www.wikimedia.es/


### Nuestros objetivos

La comunidad es lo más importante y queremos empezar una relación a largo plazo, por ello creemos que estos son los objetivos que nos pueden unir:

- Generar un espacio para la comunidad de privacidad y soberanía tecnológica (principalmente en España), uniéndola en un lugar y en un ambiente seguro.
- Colaborar y servir de nexo de unión para trabajar de manera conjunta en proyectos y ayudarnos mutuamente.
- Tratar de influir como comunidad y generar un grupo de presión en las cuestiones políticas y sociales.
- Dar visibilidad a proyectos/comunidades/personas que trabajan en torno a la privacidad y la soberanía tecnológica en nuestro país.
- Divulgar sobre privacidad y la soberanía tecnológica.

Si te sientes identificada/o con algo de todo esto, y te animas a no hacerlo sola/o, tienes aquí tu espacio. Y si crees que nos hemos saltado algo, nada está escrito en piedra.


<div style="text-align: center;">
  <b><a href="join_us">¡Únete a LaEscondite!</a></b>
</div>
