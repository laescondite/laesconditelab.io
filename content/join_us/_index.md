---
title: "¿Quieres unirte?"
# meta description
description: "Cómo participar en la comunidad"
# save as draft
draft: false
---

#### ¿Cómo participar?

**Nuestro objetivo es encontrar y conectar a todas aquellas personas inquietas que _están impulsando iniciativas_ en el mundo de la privacidad, la soberanía tecnológica y la libertad en internet** por lo que el acceso a la comunidad no es totalmente abierto.
Es por eso que si sientes que te identifica esta descripción, **nos encantaría que nos contaras porqué, y así invitarte a participar**.

**Queremos crear un entorno seguro, de gente con ganas de trabajar por cambiar las cosas de verdad.**

Debajo encontrarás un formulario en donde nos puedes contar tu motivación para querer participar. No importa quién seas o a qué te dedicas, pero si estás impulsando alguna iniciativa o participando directamente en algo relacionado con las temáticas de *LaEscondite*, no dudes en contactarnos! Y si no estás seguro, escríbenos también!


<img src="images/message.png" style="margin: auto; width: 150px;">
<script async src="https://sdk.arengu.com/forms.js"></script>
<div class="arengu-form" data-arengu-form-id="159086233600456314"></div>
<br/>

#### ¿Cómo nace LaEscondite?
*LaEscondite* nace en Granada durante la celebración de [esLibre 2019](https://eslib.re/2019/programa/), en la devroom que organizó gente de [Trackula](https://trackula.org/es/page/landing-plugin/) sobre *Privacidad, descentralización y soberanía digital*. Durante el evento, los asistentes concluimos que a pesar de identificarnos muchos con esta cuestión, nos falta un punto de encuentro común y neutral a todos. Impulsamos LaEscondite para construir este lugar.

Si tienes más curiosidad sobre cómo comenzó esto, echa un ojo a **[quiénes somos](about)**.


